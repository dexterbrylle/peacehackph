'use strict';

var config;

if (process.env.NODE_ENV === 'production') {
	config = {
		mongodb: {
			host: '127.0.0.1',
			db: 'peacehack-ph'
		},
		logger: {
			level: 'info',
			mongodb: {
				host: '127.0.0.1',
				port: '27017',
				db: 'logs',
				collection: 'peacehack-ph-logs'
			}
		}
	};
} else if (process.env.NODE_ENV === 'staging') {
	config = {
		mongodb: {
			host: '127.0.0.1',
			db: 'peacehack-ph'
		},
		logger: {
			level: 'info',
			mongodb: {
				host: '127.0.0.1',
				port: '27017',
				db: 'logs',
				collection: 'peacehack-ph-logs'
			}
		}
	};
} else {
	config = {
		mongodb: {
			host: '127.0.0.1',
			db: 'peacehack-ph'
		},
		logger: {
			level: 'info',
			mongodb: {
				host: '127.0.0.1',
				port: '27017',
				db: 'logs',
				collection: 'peacehack-ph-logs'
			}
		}
	};
}

module.exports = config;
