var verUrl = '/api';

module.exports = [
	[verUrl, 'index#index', 'get'],
	[verUrl + '/tldr', 'articles#tldr', 'get'],
	[verUrl + '/ngos/get/:ngo_name', 'ngos#get', 'get'],
	[verUrl + '/ngos/getall', 'ngos#getAll', 'get'],
	[verUrl + '/ngos/add', 'ngos#add', 'post'],
	[verUrl + '/history/get/:id', 'history#get', 'get'],
	[verUrl + '/history/getall', 'history#getAll', 'get'],
	[verUrl + '/history/add', 'history#add', 'post']
];
