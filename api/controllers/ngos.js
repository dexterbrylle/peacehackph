'use strict';

const models = require('../models');

exports.index = function (req, res) {
	res.send({
		'Message': 'index'
	});
};

exports.get = function (req, res) {
 let query = models.ngos.findOne({ ngo_name: req.param.ngo_name });

 query.exec(function (err, ngo) {
 	if (err) {
 		return res.api.error(err);
 	} else {
 		return res.api.single(ngo);
 	}
 });
};

exports.getAll = function (req, res) {
	let query = models.ngos.find();

	query.exec(function (err, ngo) {
		if (err) {
			return res.api.error(err);
		} else {
			return res.api.list(ngo);
		}
	});
};

exports.add = function (req, res) {
	let entity = new models.ngos({
		ngo_name: req.body.ngo_name,
		ngo_pretty_name: req.body.ngo_pretty_name,
		ngo_url: req.body.ngo_url,
		ngo_logo: req.body.ngo_logo,
		ngo_facebook: req.body.ngo_facebook,
		ngo_twitter: req.body.ngo_twitter,
		ngo_instagram: req.body.ngo_instagram
	});

	let query = models.ngos.findOne({ ngo_name: req.body.ngo_name });

	entity.save(function (err) {
		if (err) {
			return res.api.error(err);
		} else {
			return res.api.create(entity);
		}
	});
}
