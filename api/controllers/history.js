'use strict';

const models = require('../models');

exports.index = function (req, res) {
	res.send({
		'Message': 'index'
	});
};

exports.get = function (req, res) {
 let query = models.history.findOne({ _id: req.params.id });

 query.exec(function (err, ngo) {
 	if (err) {
 		return res.api.error(err);
 	} else {
 		return res.api.single(ngo);
 	}
 });
};

exports.getAll = function (req, res) {
	let query = models.history.find();

	query.exec(function (err, ngo) {
		if (err) {
			return res.api.error(err);
		} else {
			return res.api.list(ngo);
		}
	});
};

exports.add = function (req, res) {
	let entity = new models.history({
		article_title: req.body.article_title,
		article_url: req.body.article_url,
		history_type: req.body.history_type,
		ngo_name: req.body.ngo_name,
		shared: req.body.shared
	});

	entity.save(function (err) {
		if (err) {
			return res.api.error(err);
		} else {
			return res.api.create(entity);
		}
	});
}
