'use strict';

const request = require('request');
const fs = require('fs');
const tldr = require('node-sumuparticles');

const articles = require('./articles.json');

exports.tldr = function (req, res) {
	let article = articles[Math.floor(Math.random() * articles.length)];
	console.log(article);

	tldr.summarize(article.url, function (title, summary, failure) {
		if (failure) {
			return res.send(failure);
		} else {
			let articleJSON = {
				title: title,
				summary: summary,
				url: article.url,
				id: article.id,
				articles: articles
			};
			return res.api.single(articleJSON);
		}
	});
};
