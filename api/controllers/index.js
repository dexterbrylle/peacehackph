'use strict';

let pkg = require('../package.json');

exports.index = function (req, res) {
	res.send({
		name: pkg.name,
		version: pkg.version,
		author: pkg.author
	})
};
