'use strict';

const bcrypt = require('bcrypt-nodejs');
const mongoose = require('mongoose');
const models = require('../models');

exports.index = function (req, res) {
	res.send({
		'Message': 'index'
	});
};

exports.getUser = function (req, res) {
 let query = models.users.findOne({ _id: req.params.id });

 query.exec(function (err, user) {
 	if (err) {
 		return res.api.error(err);
 	} else {
 		return res.api.single(user);
 	}
 });
};

exports.login = function (req, res) {
	let query = models.users.findOne({ email: req.body.email });

	query.exec(function (err, user) {
		if (err) {
			log.error(err);
			return res.api.error(err);
		} else {
			if (user && bcrypt.hashSync(req.body.password, user.password)) {
				return res.api.single(user);
			} else {
				return res.api.error(err);
			}
		}
	});
};

exports.register = function (req, res) {
	let pass = bcrypt.hashSync(req.body.pass, 8);
	let entity = new models.users({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		password: pass,
		email: req.body.email,
		facebookToken: '',
		twitterToken: '',
		igToken: ''
	});

	let query = models.users.findOne({ email: req.body.email });

	query.exec(function (err, user) {
		if (err) {
			return res.api.error(err);
		} else {
			if (user) {
				log.error(err, 'User already exists');
				return res.api.error('User already exists');
			} else {
				entity.save(function (error) {
					if (error) {
						log.error(error);
						return res.api.error(error);
					} else {
						return res.api.create(entity);
					}
				});
			}
		}
	});
};
