'use strict';

const cors = require('cors');
const bodyParser = require('body-parser');
const config = require('./config');
const express = require('express');
const favicon = require('serve-favicon');
const models = require('./models');
const mongoose = require('mongoose');
const path = require('path');
const apiRes = require('api-response');
const log = require('./logger');
const expressPath = require('express-path');
const passport = require('passport');
const FacebookStrategy = require('passport-facebook');
const graph = require('facebook-complete');

mongoose.connect('mongodb://' + config.mongodb.host + '/' + config.mongodb.db);

/*passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});*/

// passport.use(new FacebookStrategy({
//   clientID: config.facebookAppId,
//   clientSecret: config.facebookAppSecret,
//   callbackURL : config.facebookCallbackURL
// }, function (accessToken, refreshToken, profile, done) {
//   console.log(accessToken);
//   graph.setAccessToken(accessToken);
//   return done(null, profile);
// }));

//Express settings
var app = express();
app.disable('x-powered-by');
app.use(cors());
app.use(bodyParser.json({
  limit: '20mb'
}));
app.use(function (req, res, next) {
  apiRes.bind(res);
  res.set('Connection', 'close');
  next();
});

if (process.env.NODE_ENV === 'development') {
  app.use(function (req, res, next) {
    console.log(req.method + ' ' + req.url);
    next();
  });
}

// app.get('/auth/facebook', passport.authenticate('facebook', {
//   scope: ['email']
// }));

// app.get('/auth/callback', passport.authenticate('facebook', {
//   failureRedirect: '/api/error'
// }), function (req, res) {
//   res.redirect('/api');
// });

expressPath(app, 'routeMap');

//Error handling
app.use(function (req, res) {
  var err = new Error('Not Found!');
  err.status = 404;
  err.method = req.method;
  err.url = req.url;
  err.remoteAddress = req.connection.remoteAddress;
  console.log(err.message, err);
  res.api.notFound();
});

app.use(function (err, req, res, next) {
  console.error(err.message, err);
  res.api.error(err.message);
});

module.exports = app;
