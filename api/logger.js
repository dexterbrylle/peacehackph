var config = require('./config'),
		winston = require('winston'),
		level = process.env.LOG_LEVEL || 'info';

winston.add(require('winston-mongodb').MongoDB, {
	db: 'mongodb://' + config.logger.mongodb.host + ':' + config.logger.mongodb.port + '/' + config.logger.mongodb.db,
	collection: config.logger.mongodb.collection,
	level: (level === 'verbose') ? 'info' : level
});

winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {
	level: level
});

module.exports = winston;
