var pkg = require('../package.json');

exports.index = function (req, res) {
	res.send({
		message: pkg.name
	});
};
