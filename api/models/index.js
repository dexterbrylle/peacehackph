module.exports = {
	users: require('./userModel'),
	articles: require('./articleModel'),
	history: require('./historyModel'),
	ngos: require('./ngoModel')
};
