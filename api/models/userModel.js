'use strict';

const mongoose = require('mongoose');

let Schema = mongoose.Schema;
let ObjectId = Schema.ObjectId;


let schema = new Schema({
	firstName: {
		type: String,
		required: true
	},
	lastName: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true
	},
	facebookToken: {
		type: String
	},
	twitterToken: {
		type: String
	},
	igToken: {
		type: String
	}
});

schema.set('autoIndex', true);

module.exports = mongoose.model('users', schema);
