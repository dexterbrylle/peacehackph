'use strict';

const mongoose = require('mongoose');

let Schema = mongoose.Schema;
let ObjectId = Schema.ObjectId;

let schema = new Schema({
	article_title: {
		type: String
	},
	article_url: {
		type: String
	},
	history_type: {
		type: String
	},
	ngo_name: {
		type: String
	},
	shared: {
		type: Array
	}
});

schema.set('autoIndex', true);

module.exports = mongoose.model('history', schema);
