'use strict';

const mongoose = require('mongoose');

let Schema = mongoose.Schema;
let ObjectId = Schema.ObjectId;

let schema = new Schema({

});

schema.set('autoIndex', true);

module.exports = mongoose.model('articles', schema);
