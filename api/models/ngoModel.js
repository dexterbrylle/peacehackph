'use strict';

const mongoose = require('mongoose');

let Schema = mongoose.Schema;
let ObjectId = Schema.ObjectId;

let schema = new Schema({
	ngo_name: {
		type: String
	},
	ngo_pretty_name: {
		type: String
	},
	ngo_url: {
		type: String
	},
	ngo_logo: {
		type: String
	},
	ngo_facebook: {
		type: String
	},
	ngo_twitter: {
		type: String
	},
	ngo_instagram: {
		type: String
	}
});

schema.set('autoIndex', true);

module.exports = mongoose.model('ngos', schema);
