angular.module('starter.controllers', [])

.controller('IntroCtrl', function ($scope, $ionicSlideBoxDelegate, $location) {
  $scope.options = {
    loop: false,
    effect: 'fade',
    speed: 500
  };

  $scope.$on('$ionicSlides.sliderInitialized', function (event, data) {
    $scope.slider = data.slider;
  });

  $scope.$on('$ionicSlides.slideChangeStart', function (event,data) {
    console.log('Changing slides...');
  });
})

.controller('LoginCtrl', function ($scope, $location, $ionicLoading, $ionicPopup, Users) {
  $scope.login = function () {
    $ionicLoading.show({
      template: 'Logging in...'
    });
    $location.path('/tab/home');
    $ionicLoading.hide();
  };
})

.controller('HomeCtrl', function ($scope, $location, $ionicLoading, $ionicPopup, $ionicScrollDelegate, $timeout, $ionicModal, Articles) {
  $scope.$on('$ionicView.enter', function (e) {
    $ionicLoading.show({
      template: '<ion-spinner></ion-spinner>' + '\n\n Getting article...'
    });

    $timeout(function () {
      Articles.get(function (err, article) {
        console.log(article.data.data);
        $scope.article = article.data.data;
        $ionicLoading.hide();
        $scope.openArticle = function () {
          window.open($scope.article.url, '_blank');
        };
      });
    }, 1000);
  });

  $scope.article = null;

  $scope.doRefresh = function () {
    $timeout(function () {
      Articles.get(function (err, article) {
        console.log(article.data.data);
        $scope.article = article.data.data;
        $ionicLoading.hide();
        $scope.openArticle = function () {
          window.open(article.url, '_blank');
        };
      });
      $scope.$broadcast('scroll.refreshComplete');
    }, 1000);
  };

  $scope.onComplete = function () {
    var scrollBottom = $ionicScrollDelegate.getScrollPosition().top;
    showConfirm();
  };

  $scope.showConfirm = function () {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Give PEACE a chance.',
      template: 'Want to know how you can help out in this time of need? Your OK will go a long way.',
      scope: $scope,
      cssClass: 'animated slideInUp'
    });

    confirmPopup.then(function (res) {
      if (res) {
        $location.path('/ngo');
      } else {
        console.log('No');
      }
    });
  };
 })

.controller('HistoryCtrl', function ($scope, $http, History) {
  $scope.history = [];

  History.get(function (err, history) {
    console.log(history);
    $scope.history = history;
  });
})

.controller('NgoCtrl', function ($scope, $http, $ionicLoading, $location, $ionicPopup, $ionicScrollDelegate, $timeout, Ngos) {
  $scope.ngos = [];
  $scope.allngos = []
  $scope.allngos = Ngos.all();

  $ionicLoading.show({
    template: '<ion-spinner></ion-spinner>' + '\n\n Getting NGOs...'
  });
  Ngos.getAll(function (err, ngo) {
    console.log(ngo);
    $scope.ngos = ngo;
    $ionicLoading.hide();
  });

  $timeout(function () {
   $scope.ngoDetail = $location.path('/detail');
  }, 15000);
})
.controller('DetailCtrl', function ($scope, $stateParams, $location, $timeout, $ionicPopup, Ngos) {
  // Triggered on a button click, or some other target
  $scope.showPopup = function() {
    $scope.data = {};

    // An elaborate, custom popup
    var donatePopup = $ionicPopup.show({
      template: '<input type="text" ng-model="data.amount">',
      title: 'Donate Financial Assistance',
      subTitle: 'Your help will go a long way',
      scope: $scope,
      buttons: [
        { text: 'Cancel' },
        {
          text: '<b>Donate</b>',
          type: 'button-calm',
          onTap: function(e) {
            if (!$scope.data.amount) {
              //don't allow the user to close unless he enters wifi password
              e.preventDefault();
            } else {
              return $scope.data.amount;
            }
          }
        }
      ]
    });

    donatePopup.then(function(res) {
      console.log('Tapped!', res);
      window.alert('Thank you!');
      donatePopup.close(); //close the popup after 3 seconds for some reason

    });
    $timeout(function () {
      $location.path('/tab/home');
    }, 5000);
   };
})
.controller('ProfileCtrl', function ($scope) {

});
