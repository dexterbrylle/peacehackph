var API_URL = 'http://192.168.43.159:5000/api';
// var API_URL = 'http://localhost:5000/api';
angular.module('starter.services', [])

.factory('Users', function ($http) {
  var currentUser = null;

  return {
    auth: function (email, password, callback) {
      $http.post(API_URL + '/users/auth', {
        email: email,
        password: password
      })
        .then(function (response) {
          console.log(response.data);
          currentUser = response.data.data;
          callback(null, currentUser);
        }, function (response) {
          console.error(response);
          callback();
        });
    },
    register: function (email, password, firstname, lastname, callback) {
      $http.post(API_URL + '/users/register', {
        email: email,
        password: password,
        firstname: firstname,
        lastname: lastname
      })
        .then(function (response) {
          currentUser = response.data.data;
          callback(null, response);
        }, function (response) {
          console.error(response);
          callback();
        });
    },
    self: function (callback) {
      return callback(null, currentUser);
    },
    set: function (user, callback) {
      currentUser = user;
      return callback(null, user)
    }
  };
})

.factory('Ngos', function ($http) {
  var ngos = [];

  return {
    getAll: function (cb) {
      $http.get(API_URL + '/ngos/getall')
        .then(function (response) {
          ngos = response.data.data;
          cb(null, ngos);
        }, function (response) {
          console.error(response);
          cb();
        });
    },
    all: function () {
      return ngos;
    },
    get: function (ngo_name) {
      for (var i = 0; i< ngos.length; i++) {
        if (ngos[i].ngo_name === ngo_name) {
          return ngos[i];
        }
      }
      return null;
    }
  };
})

.factory('History', function ($http) {
  var history = [];

  return {
    get: function (cb) {
      $http.get(API_URL + '/history/getall')
        .then(function (response) {
          history = response.data.data;
          cb(null, history);
        }, function (response) {
          console.error(response);
          cb();
        });
    },
    all: function () {
      return history;
    }
  }
})

.factory('Articles', function ($http) {
  var previousArticle = null;
  var newArticle = null;

  return {
    get: function (cb) {
      $http.get(API_URL + '/tldr')
        .then(function (response) {
          console.log(response);
          cb(null, response);
        }, function (response) {
          console.error(response);
          cb();
        });
    }
  }
});
